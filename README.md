Plugin for extended bubble alerts.

Download: http://arcturus.wf/mirrors/bubblealerts.jar

HowTo:
Added the images to the c_images/notifications folder.

This plugin has bubbles for:

- Receiving pixels
- Receiving credits
- Buying furniture.
- Buying limited furniture.