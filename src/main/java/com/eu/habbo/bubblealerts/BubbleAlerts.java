package com.eu.habbo.bubblealerts;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.items.Item;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.habbohotel.users.HabboItem;
import com.eu.habbo.messages.outgoing.generic.alerts.BubbleAlertComposer;
import com.eu.habbo.messages.outgoing.generic.alerts.BubbleAlertKeys;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorConfigUpdatedEvent;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadedEvent;
import com.eu.habbo.plugin.events.users.UserCreditsEvent;
import com.eu.habbo.plugin.events.users.UserPointsEvent;
import com.eu.habbo.plugin.events.users.catalog.UserCatalogItemPurchasedEvent;
import gnu.trove.map.hash.THashMap;
import gnu.trove.procedure.TObjectProcedure;

public class BubbleAlerts extends HabboPlugin implements EventListener
{

    public BubbleAlerts()
    {
        Emulator.getPluginManager().registerEvents(this, this);
    }
    @Override
    public void onEnable()
    {

        if (Emulator.isReady)
        {
            init();
        }
        Emulator.getLogging().logStart("[BubbleAlerts] Plugin Enabled!");
    }

    @Override
    public void onDisable()
    {
        Emulator.getLogging().logShutdownLine("[BubbleAlerts] Plugin Stopped!");
    }

    @Override
    public boolean hasPermission(Habbo habbo, String s)
    {
        return false;
    }

    @EventHandler
    public static void onEmulatorLoaded(EmulatorLoadedEvent event)
    {
        init();
    }

    @EventHandler
    public static void onUserCreditsReceived(UserCreditsEvent event)
    {
        if (event.credits > 0)
        {
            THashMap<String, String> credits_keys = new THashMap<String, String>();
            credits_keys.put("display", "BUBBLE");
            credits_keys.put("image", "${image.library.url}notifications/credits_image.png");
            credits_keys.put("message", Emulator.getTexts().getValue("commands.generic.cmd_credits.received").replace("%amount%", event.credits + ""));
            credits_keys.put("count", event.credits + "");
            event.habbo.getClient().sendResponse(new BubbleAlertComposer("receivedcredits", credits_keys));
        }
    }

    @EventHandler
    public static void onUserPointsReceived(UserPointsEvent event)
    {
        if (event.points > 0 && event.type != 5)
        {
            THashMap<String, String> points_keys = new THashMap<String, String>();
            points_keys.put("display", "BUBBLE");
            points_keys.put("image", "${image.library.url}notifications/points_" + event.type + ".png");
            points_keys.put("message", Emulator.getTexts().getValue("commands.generic.cmd_points.received").replace("%amount%", event.points + "").replace("%type%", Emulator.getTexts().getValue("seasonal.name." + event.type)));
            points_keys.put("count", event.points + "");
            points_keys.put("type", event.type + "");
            event.habbo.getClient().sendResponse(new BubbleAlertComposer("receivedpoints", points_keys));
        }
    }

    @EventHandler
    public static void onFurnitureBought(final UserCatalogItemPurchasedEvent event)
    {
        if (event.catalogItem.getBaseItems().size() == 1)
        {
            if (Emulator.getConfig().getBoolean("bubblealerts.notif_purchase.limited") && !event.catalogItem.isLimited())
                return;

            event.catalogItem.getBaseItems().forEach(new TObjectProcedure<Item>()
            {
                @Override
                public boolean execute(Item baseItem)
                {
                    if (baseItem != null && !baseItem.getName().equalsIgnoreCase(baseItem.getFullName()))
                    {
                        HabboItem firstItem = (HabboItem)event.itemsList.toArray()[0];
                        THashMap<String, String> points_keys = new THashMap<String, String>();
                        points_keys.put("display", "BUBBLE");
                        points_keys.put("image", Emulator.getConfig().getValue("path.furniture.icons") + "/" + baseItem.getName() + "_icon.png");
                        points_keys.put("message",
                                Emulator.getTexts().getValue("generic.furniture.bought")
                                        .replace("%is_limited%", event.catalogItem.isLimited() ? " limited " : " ")
                                        .replace("%limited_display%", event.catalogItem.isLimited() ? " " + firstItem.getLimitedSells() + "/" + event.catalogItem.getLimitedStack() : "")
                                        .replace("%itemname%", baseItem.getFullName())
                                        .replace("%amount%", event.itemsList.size() == 1 ? "a" : event.itemsList.size() + ""));
                        event.habbo.getClient().sendResponse(new BubbleAlertComposer("boughtfurniture", points_keys));
                    }

                    return false;
                }
            });
        }
    }

    private static void init()
    {
        Emulator.getConfig().register("path.furniture.icons", "${image.library.url}/icons/");
        Emulator.getConfig().register("bubblealerts.notif_purchase.limited", "0");
        Emulator.getTexts().register("generic.furniture.bought", "You just purchased %amount%%is_limited%%itemname%%limited_display%!");
    }
}